#include "bmbflauncherwindow.h"
#include "ui_bmbflauncherwindow.h"

#include <string>
#include <fstream>
#include <thread>
#include <chrono>


BMBFLauncherWindow::BMBFLauncherWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::BMBFLauncherWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("BMBF Launcher - v1.0.0");

    std::string ip;
    std::string ipRAW;
    std::ifstream ipTxt("ip.txt");
    if (ipTxt.is_open()) {
        while (std::getline(ipTxt, ipRAW)) {
            ip += ipRAW;
        }
        ipTxt.close();
    }
     ui->IP_Text->setText(QString::fromUtf8(ip.c_str()));
}

BMBFLauncherWindow::~BMBFLauncherWindow()
{
    delete ui;
}


void BMBFLauncherWindow::on_Save_IP_clicked()
{
    std::ofstream ipTxt("ip.txt");
    ipTxt << this->ui->IP_Text->text().toStdString();
    ipTxt.close();
    this->ui->Save_Notice->setText("IP Saved...");
}

QString ToasterLog;

void BMBFLauncherWindow::on_Start_Button_clicked()
{


    this->ui->Start_Button->setEnabled(false);

    QTextBrowser* Toaster = this->ui->Toaster;

    Toaster->setText(ToasterLog += "\n\n------------------");

    Toaster->setText(ToasterLog += "\nGrabbing IP from file...");

    std::string content;
    std::ifstream ipTxt("ip.txt");
    if (ipTxt.is_open()) {
        std::string line;
        while (std::getline(ipTxt, line)) {
            content += line;
        }
        ipTxt.close();

        Toaster->setText(ToasterLog += "\nLaunching BMBF in headset...");

        system("adb shell am start com.weloveoculus.BMBF");
        std::this_thread::sleep_for(std::chrono::milliseconds(5000));

        Toaster->setText(ToasterLog += "\nConstructing Link...");

        std::string link = content += ":50000";
        std::string command = "start http://";
        command += link;

        Toaster->setText(ToasterLog += "\nOpening In Browser...");

        system(command.c_str());

        Toaster->setText(ToasterLog += "\nDone...");

        this->ui->Start_Button->setEnabled(true);
    }
    else Toaster->setText(ToasterLog += "Error Reading IP: File Not Found\nCheck it's saved");
}
