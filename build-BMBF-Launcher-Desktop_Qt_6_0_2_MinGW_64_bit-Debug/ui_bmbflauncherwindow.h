/********************************************************************************
** Form generated from reading UI file 'bmbflauncherwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BMBFLAUNCHERWINDOW_H
#define UI_BMBFLAUNCHERWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BMBFLauncherWindow
{
public:
    QWidget *centralwidget;
    QLineEdit *IP_Text;
    QPushButton *Save_IP;
    QTextBrowser *Toaster;
    QPushButton *Start_Button;
    QLabel *Notice;
    QLabel *Instruction;
    QLabel *Save_Notice;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *BMBFLauncherWindow)
    {
        if (BMBFLauncherWindow->objectName().isEmpty())
            BMBFLauncherWindow->setObjectName(QString::fromUtf8("BMBFLauncherWindow"));
        BMBFLauncherWindow->resize(485, 527);
        BMBFLauncherWindow->setStyleSheet(QString::fromUtf8("background-color:rgb(33, 33, 33);\n"
"border-color: rgb(255, 255, 255);\n"
"alternate-background-color: rgb(195, 195, 195);\n"
"border-top-color: rgb(255, 255, 255);\n"
"border-color: rgb(255, 255, 255);\n"
"gridline-color: rgb(255, 255, 255);\n"
"selection-color: rgb(255, 255, 255);\n"
"selection-background-color: rgb(255, 255, 255);\n"
"\n"
"QPushButton {\n"
"            color: white;\n"
"            border-top: 3px transparent;\n"
"            border-bottom: 3px transparent;\n"
"            border-right: 10px transparent;\n"
"            border-left: 10px transparent;\n"
" }"));
        centralwidget = new QWidget(BMBFLauncherWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        IP_Text = new QLineEdit(centralwidget);
        IP_Text->setObjectName(QString::fromUtf8("IP_Text"));
        IP_Text->setGeometry(QRect(30, 30, 201, 21));
        IP_Text->setStyleSheet(QString::fromUtf8("background-color: rgb(239, 239, 239);\n"
"color: rgb(0, 0, 0);"));
        Save_IP = new QPushButton(centralwidget);
        Save_IP->setObjectName(QString::fromUtf8("Save_IP"));
        Save_IP->setGeometry(QRect(240, 30, 75, 23));
        Save_IP->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        Toaster = new QTextBrowser(centralwidget);
        Toaster->setObjectName(QString::fromUtf8("Toaster"));
        Toaster->setGeometry(QRect(30, 260, 421, 192));
        Toaster->setStyleSheet(QString::fromUtf8("background-color: rgb(84, 84, 84);\n"
"background-color: rgb(235, 235, 235);"));
        Start_Button = new QPushButton(centralwidget);
        Start_Button->setObjectName(QString::fromUtf8("Start_Button"));
        Start_Button->setGeometry(QRect(140, 170, 191, 61));
        Start_Button->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        Notice = new QLabel(centralwidget);
        Notice->setObjectName(QString::fromUtf8("Notice"));
        Notice->setGeometry(QRect(30, 70, 261, 20));
        Notice->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        Instruction = new QLabel(centralwidget);
        Instruction->setObjectName(QString::fromUtf8("Instruction"));
        Instruction->setGeometry(QRect(170, 120, 141, 21));
        Instruction->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        Save_Notice = new QLabel(centralwidget);
        Save_Notice->setObjectName(QString::fromUtf8("Save_Notice"));
        Save_Notice->setGeometry(QRect(330, 33, 71, 16));
        Save_Notice->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        BMBFLauncherWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(BMBFLauncherWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 485, 21));
        BMBFLauncherWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(BMBFLauncherWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        BMBFLauncherWindow->setStatusBar(statusbar);

        retranslateUi(BMBFLauncherWindow);

        QMetaObject::connectSlotsByName(BMBFLauncherWindow);
    } // setupUi

    void retranslateUi(QMainWindow *BMBFLauncherWindow)
    {
        BMBFLauncherWindow->setWindowTitle(QCoreApplication::translate("BMBFLauncherWindow", "BMBFLauncherWindow", nullptr));
        Save_IP->setText(QCoreApplication::translate("BMBFLauncherWindow", "Save", nullptr));
        Start_Button->setText(QCoreApplication::translate("BMBFLauncherWindow", "Start", nullptr));
        Notice->setText(QCoreApplication::translate("BMBFLauncherWindow", "<html><head/><body><p><span style=\" font-size:9pt; color:#ffffff;\">Make sure you read the instructions on GitHub</span></p></body></html>", nullptr));
        Instruction->setText(QCoreApplication::translate("BMBFLauncherWindow", "<html><head/><body><p><span style=\" font-size:12pt; color:#ffffff;\">Press Start to Begin</span></p></body></html>", nullptr));
        Save_Notice->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class BMBFLauncherWindow: public Ui_BMBFLauncherWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BMBFLAUNCHERWINDOW_H
