#include "bmbflauncherwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    BMBFLauncherWindow w;
    w.show();
    return a.exec();
}
